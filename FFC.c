#include <stdio.h>
#include <math.h>

int main()
{
	int s; // 메뉴 선택
	double ccdh, ccdv; //ccd가로,세로
	double need, needr; //필요 수치
	double r; //결과

	//기본출력
	system("clear");
                                            
	printf("#### 이 프로그램의 저작권은 ⓒLibert Sin 에 있습니다. ####\n 자유로운 배포·수정은 허용하지만 원저작자와 출처를 표시해야 합니다.\n http://photoguraphy.com/\n#############\n\n\n");


	printf("## 35mm 풀프레임규격(1:1 135)의 ccd 가로 세로 길이는 36 X 24 입니다. ## \n\n");
	printf("  1. 초점거리 -> 화각\n  2. 화각 -> 초점거리\n >");
	scanf("%d",&s); getchar();

	printf("\n ccd의 가로 길이 (mm) \n > ");
	scanf("%lf",&ccdh); getchar();

	printf("\n ccd의 세로 길이 (mm)\n > ");
	scanf("%lf",&ccdv); getchar();


	//화각->초점거리  

if(s==1)
{
	printf("\n 초점거리를 구하고 싶은 화각(˚)\n >");
	scanf("%lf",&need); getchar();
	
	needr=need*M_PI/180;

	r=(sqrt(ccdh*ccdh+ccdv*ccdv)/(2*tan(needr/2)));
	printf("\n%f\n\n",r);
	printf("입력하신 %f X %f ccd에서 %f˚의 화각은 %f mm의 초점거리를 가집니다.\n",ccdh,ccdv,need,r);
	
	getchar();
}


	//초점거리->화각
else if(s==2)
{
	printf("화각을 구하고 싶은 초점거리(mm)\n >");
	scanf("%lf",&need); getchar();

	r=2*atan(sqrt(ccdh*ccdh+ccdv*ccdv)/(2*need));
	r=r*180/M_PI;
	printf("\n%f\n\n",r);
	printf("입력하신 %f X %f ccd에서 %f mm의 초점거리는 %f˚의 화각을 가집니다.\n",ccdh,ccdv,need,r);
	getchar();
}

else
{
	printf("잘못된 입력입니다\n");
	getchar();
}

}